package test;




import java.time.LocalDate;
import static org.junit.Assert.*;

import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;


public class Altacliente {

	
	/*
	 * Insertamos un cliente que ya existe
	 */
	@Test
	public void insetarClienteRepetido() {
		GestorContabilidad gestorpruebas = new GestorContabilidad();
		Cliente clientebuscado = new Cliente("sdfsdf", "ramos", LocalDate.now());
		Cliente clientebuscado2 = new Cliente("464564", "jose", LocalDate.now());
		Cliente clientebuscado3 = new Cliente("1234F", "alfredo", LocalDate.now());
		gestorpruebas.getvClientes().add(clientebuscado);
		gestorpruebas.getvClientes().add(clientebuscado2);
		gestorpruebas.getvClientes().add(clientebuscado3);
		
		Cliente uncliente = new Cliente("1234F", "Paquito", LocalDate.now());
		
		gestorpruebas.altaCliente(uncliente);
		Cliente esperado= new Cliente("","",LocalDate.now());
		
		for (int j = 0; j < gestorpruebas.getvClientes().size(); j++) {
			if(uncliente.getDni().equals(gestorpruebas.getvClientes().get(j).getDni())){
			esperado=gestorpruebas.getvClientes().get(j);
			}
		}
		assertNotEquals(esperado, uncliente);
	}

	/*
	 * Insertamos un cliente Nuevo
	 */
	@Test
	public void InsertarClienteNuevo() {
			GestorContabilidad gestorpruebas = new GestorContabilidad();
			Cliente clientebuscado = new Cliente("sdfsdf", "ramos", LocalDate.now());
			Cliente clientebuscado2 = new Cliente("464564", "ruan", LocalDate.now());
			Cliente clientebuscado3 = new Cliente("1234F", "alfredo", LocalDate.now());
			gestorpruebas.getvClientes().add(clientebuscado);
			gestorpruebas.getvClientes().add(clientebuscado2);
			gestorpruebas.getvClientes().add(clientebuscado3);
			
			Cliente uncliente = new Cliente("12345678F", "Paquito", LocalDate.now());
			
			gestorpruebas.altaCliente(uncliente);
			
			Cliente esperado= new Cliente("","",LocalDate.now());
			
			for (int j = 0; j < gestorpruebas.getvClientes().size(); j++) {
				if(uncliente.getDni().equals(gestorpruebas.getvClientes().get(j).getDni())){
				esperado=gestorpruebas.getvClientes().get(j);
				}
			}
			assertEquals(esperado, uncliente);
	}
	
}
