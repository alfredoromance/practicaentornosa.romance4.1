package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class EliminarFacturayCliente {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		Cliente cli1 = new Cliente("1234A");
		
	
	
	GestorContabilidad cliente = new GestorContabilidad();
		cliente.getvClientes().add(cli1);
		
		GestorContabilidad factura = new GestorContabilidad();
		Factura fac1 = new Factura("122A");
		factura.getvFacturas().add(fac1);
	}

	
	
	@Test
	public void eliminarFactura() {
		
		String cod = "122A";
		
		GestorContabilidad eliminarFactura = new GestorContabilidad();
		
		eliminarFactura.eliminarCliente(cod);
		
		assertNull(cod);
		
		
		
		
	}
	public void eliminarCliente() {
		
		
		String dni= "1234A";
		
		GestorContabilidad eliminarcliente = new GestorContabilidad();
		
		eliminarcliente.eliminarCliente(dni);
		
		assertNull(dni);
		
		
		
		
	}

}
