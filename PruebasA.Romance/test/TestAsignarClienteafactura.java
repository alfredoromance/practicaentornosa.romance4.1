package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestAsignarClienteafactura {
	
	@BeforeClass
	public static void generardatos(){
		
		Factura unafac= new Factura("12");
		Factura unafac1= new Factura("123");
		Factura unafac2= new Factura("124");
		Factura unafac3= new Factura("125");
		
	}
	/**
	 * Comprueba que se ayan asignado correctamente la factura
	 */
	@Test
	public void TestAsignarcloenteafactura(){
		
		GestorContabilidad esperado= new GestorContabilidad();
		GestorContabilidad actual= new GestorContabilidad();
		Cliente uncli = new Cliente("1234h","Alfredo", LocalDate.now());
		esperado.AsignarFacturaACliente("1234h", "124");
		actual.AsignarFacturaACliente("1234h", "124");
		assertEquals(esperado, actual);
		 
		
		
	}
}
