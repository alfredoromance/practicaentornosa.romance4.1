package test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Factura;
import clases.GestorContabilidad;

public class TestFacturaMasCara {

	static GestorContabilidad gestor= new GestorContabilidad();
	/*
	 * Generamos 3 facturas
	 */
	@BeforeClass
	public static void generarFacturas(){
		
		Factura unafactura= new Factura(2, 1);
		Factura unafactura1= new Factura(2, 5);
		Factura unafactura2= new Factura(2, (float) 2.5);
		
		gestor.getvFacturas().add(unafactura);
		gestor.getvFacturas().add(unafactura1);
		gestor.getvFacturas().add(unafactura2);
	}
	
	/*
	 * Factura mas cara
	 */
	@Test
	public void facturaMasCara(){
		
	Factura esperada= gestor.facturaMascara();
	Factura unafactura1=gestor.getvFacturas().get(1);
	assertEquals(unafactura1, esperada);
	}
	/*
	 * Facturacion de un a�o
	 */
	@Test
	public void FacturaAnual(){
		Factura unafactura= new Factura(2, 1,"1990");
		Factura unafactura1= new Factura(2, 5,"1990");
		Factura unafactura2= new Factura(2, (float) 2.5,"2000");
		
		gestor.getvFacturas().add(unafactura);
		gestor.getvFacturas().add(unafactura1);
		gestor.getvFacturas().add(unafactura2);
		float resultado;
		resultado=gestor.calcularFacAnual("1990");
		float esperado=12;
		assertEquals(esperado, resultado, 0);
	}
}
