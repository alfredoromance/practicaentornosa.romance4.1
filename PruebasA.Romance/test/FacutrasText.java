package test;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Factura;
import clases.GestorContabilidad;

public class FacutrasText {

	static Factura facturaprueba;
	static GestorContabilidad ungestor;
	/*
	 * Generamos el gestor
	 */
	@BeforeClass
	public static void prepararClasePruebas(){
		
		ungestor = new GestorContabilidad();
	}
	/*
	 * Buscamos una factura
	 */
	@Test
	public void BuscarFactura(){
		Factura Actual= new Factura();
		Actual=ungestor.buscarFactura("1243");
		
		assertNull(Actual);
	}
	/*
	 * Buscamos una factura inexistente
	 */
	@Test
	public void buscarFacturainexistente(){
		Factura Actual= new Factura();
		Actual=ungestor.buscarFactura("1243");
		
		assertNull(Actual);
	}
	/*
	 * Buscamos factura existente
	 */
	@Test
	public void buscarFacturaExistente(){
		Factura Actual= new Factura();
		Actual.setCodigoFactura("123456");
		Factura esperada = new Factura();
		ungestor.getvFacturas().add(Actual);
		esperada=ungestor.buscarFactura("123456");
		
		assertEquals(Actual, esperada);
		
		
	}
	/*
	 * Buscamos factura existente entre muchas
	 */
	@Test
	public void buscarFacturaExistenteenvarias(){
		Factura Actual= new Factura();
		Actual.setCodigoFactura("123456");
		
		Factura Actual1= new Factura();
		Actual1.setCodigoFactura("1234567");
		
		Factura esperada = new Factura();
		ungestor.getvFacturas().add(Actual);
		ungestor.getvFacturas().add(Actual1);
		esperada=ungestor.buscarFactura("1234567");
		
		assertEquals(Actual1, esperada);
		
		
	}
	/*
	 * Calcular precio de producto
	 */
	@Test
	public void testcalcularprecioProducto1() {
		
		
		facturaprueba.setCantidad(3);
		facturaprueba.setPrecioUnidad(2.5F);
		
		float esperado = 7.5F;
		float actual = facturaprueba.calcularPrecioTotal();
		assertEquals(esperado, actual,0.0);
	}
	/*
	 * Calculamos precio del producto
	 */
	@Test
	public void testcalcularprecioProducto2() {
		
		facturaprueba.setCantidad(0);
		facturaprueba.setPrecioUnidad(0.5F);
		
		float esperado = 0F;
		float actual = facturaprueba.calcularPrecioTotal();
		assertEquals(esperado, actual,0.0);
	}
	/*
	 * Factura mas cara nula
	 */
	@Test
	public void facturaMasCaranula(){
		Factura resultado=ungestor.facturaMascara();
		assertNull(resultado);
	}
	/*
	 * Facuracion anual nula
	 */
	@Test
	public void facturacionanual(){
		Factura unafactura= new Factura(1, 1999);
		ungestor.getvFacturas().add(unafactura);
		float facanual=ungestor.calcularFacAnual("1990");
		assertEquals(facanual, 0, 0);
	}

}
