package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class TextClientes {

	/*
	 * Generamos el gestor y las factura
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		GestorContabilidad gestorpruebas = new GestorContabilidad();
		Cliente clientebuscado = new Cliente("sdfsdf", "ramos", LocalDate.parse("2001-03-13"));
		Cliente clientebuscado2 = new Cliente("464564", "ruan", LocalDate.parse("1998-03-13"));
		Cliente clientebuscado3 = new Cliente("1234F", "Juan", LocalDate.now());
		gestorpruebas.getvClientes().add(clientebuscado);
		gestorpruebas.getvClientes().add(clientebuscado2);
		gestorpruebas.getvClientes().add(clientebuscado3);
	}
	/*
	 * Buscamos cliente por DNI nulo
	 */
	@Test
	public void test() {
		GestorContabilidad gestorpruebas = new GestorContabilidad();
		
		String dni="2334242";
		Cliente actual = gestorpruebas.buscarCliente(dni);
		assertNull(actual);
	}
	/*
	 * Buscamos cliente con cliente existente
	 */
	@Test
	public void testBuscarClienteexistenteconClientes(){
		GestorContabilidad gestorpruebas = new GestorContabilidad();
		Cliente clientebuscado = new Cliente("sdfsdf", "ramos", LocalDate.now());
		Cliente clientebuscado2 = new Cliente("464564", "ruan", LocalDate.now());
		Cliente clientebuscado3 = new Cliente("1234F", "Juan", LocalDate.now());
		gestorpruebas.getvClientes().add(clientebuscado);
		gestorpruebas.getvClientes().add(clientebuscado2);
		gestorpruebas.getvClientes().add(clientebuscado3);
		
		Cliente Actual= gestorpruebas.buscarCliente("1234F");
		
		assertEquals(clientebuscado3, Actual);
	}
	/*
	 * Buscamos cliente inexistente en array con clientes
	 */
	@Test
	public void testBuscarClienteineexistenteconClientes(){
		GestorContabilidad gestorpruebas = new GestorContabilidad();
		Cliente clientebuscado = new Cliente("1234F", "Juan", LocalDate.now());
		
		gestorpruebas.getvClientes().add(clientebuscado);
		
		Cliente Actual= gestorpruebas.buscarCliente("64f");
		
		assertNull(Actual);
	}
	
	
	/*
	 * Buscamos al cliente mas antiguo
	 */
	@Test
	public void textClienteMasAntiguo() throws Exception{
		GestorContabilidad gestorpruebas = new GestorContabilidad();
		Cliente clientebuscado = new Cliente("sdfsdf", "ramos", LocalDate.parse("2001-03-13"));
		Cliente clientebuscado2 = new Cliente("464564", "ruan", LocalDate.parse("1998-03-13"));
		Cliente clientebuscado3 = new Cliente("1234F", "Juan", LocalDate.now());
		gestorpruebas.getvClientes().add(clientebuscado);
		gestorpruebas.getvClientes().add(clientebuscado2);
		gestorpruebas.getvClientes().add(clientebuscado3);
		Cliente resultado;
		resultado=gestorpruebas.clienteMasAntiguo();
		assertEquals(clientebuscado2, resultado);
	}
	

}
